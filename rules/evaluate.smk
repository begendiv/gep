localrules: symlink_UnzippedFastq_R1_HiC, 					\
			symlink_UnzippedFastq_R2_HiC, 					\
			symlink_UnzippedFasta_asm1, 					\
			symlink_UnzippedFasta_asm2, 					\
			KeyResults_GenomescopeProfiles, 				\
			KeyResults_statistics,                          \
			KeyResults_merqury,								\
			KeyResults_busco,		                        \
			KeyResults_smudgeplot,				     		\
			all_metrics_table_asm1,                   	    \
			all_metrics_table_asm2,               		    \
			IndividualResults_md, 							\
			PretextMaps_md, 								\
			Reports_md, 									\
			Reports_pdf,									\
			ColouredTable_html, 							\
			HeatmapTable_html,		  						\
			BothTables_pdf,									\
			ConcatAll_pdfs,									\
			symlink_genomescope,					     	\
			symlink_smudgeplot,    						    \
			all_metrics_final_table,                        \
			Table_md_all_metrics,                           \
			compute_kmer_coverage_ear,                      \
			make_ear

def HiC_R1_gzipped(wildcards):
	return yesGzip_HiC_R1.loc[(wildcards.asmID), "HiC_R1"]

rule unzipFastq_R1_HiC:
	input:
		assembly=HiC_R1_gzipped,
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R1.fastq")),
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/pigzUnzip.HiC.R1.{asmID}.log")
	conda:
		os.path.join(workflow.basedir, "envs/UNZIP_and_QC.yaml")
	threads:
		resource['unzipFastq_R1_HiC']['threads']
	resources:
		mem_mb=resource['unzipFastq_R1_HiC']['mem_mb'],
		time=resource['unzipFastq_R1_HiC']['time'],
	shell:
		"""
		pigz -p {threads} -c -d -k {input.assembly} > {output} 2> {log}
		"""

def HiC_R1_unzipped(wildcards):
	return noGzip_HiC_R1.loc[(wildcards.asmID), "HiC_R1"]

rule symlink_UnzippedFastq_R1_HiC:
	input:
		assembly=HiC_R1_unzipped,
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R1.fastq")),
	container:
		None
	shell:
		"""
		ln -sf {input} {output}
		"""


def HiC_R2_gzipped(wildcards):
	return yesGzip_HiC_R2.loc[(wildcards.asmID), "HiC_R2"]

rule unzipFastq_R2_HiC:
	input:
		assembly=HiC_R2_gzipped,
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R2.fastq")),
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/pigzUnzip..HiC.R2.{asmID}.log")
	conda:
		os.path.join(workflow.basedir, "envs/UNZIP_and_QC.yaml")
	threads:
		resource['unzipFastq_R2_HiC']['threads']
	resources:
		mem_mb=resource['unzipFastq_R2_HiC']['mem_mb'],
		time=resource['unzipFastq_R2_HiC']['time']
	shell:
		"""
		pigz -p {threads} -c -d -k {input.assembly} > {output} 2> {log}
		"""

def HiC_R2_unzipped(wildcards):
	return noGzip_HiC_R2.loc[(wildcards.asmID), "HiC_R2"]

rule symlink_UnzippedFastq_R2_HiC:
	input:
		assembly=HiC_R2_unzipped,
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R2.fastq")),
	container:
		None
	shell:
		"""
		ln -sf {input} {output}
		"""


rule indexFasta_asm1:
	input:
		assembly_asm1=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.fai")
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/indexASM.asm1.{asmID}.log")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['indexFasta_PRI']['threads']
	resources:
		mem_mb=resource['indexFasta_PRI']['mem_mb'],
		time=resource['indexFasta_PRI']['time']
	shell:
		"""
		(bwa-mem2 index {input.assembly_asm1}) &> {log}
		(samtools faidx {input.assembly_asm1}) &>> {log}
		"""

def asm2File(wildcards):
	if samples.loc[(wildcards.asmID), "ALT_present"] == True:
		return os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta")
	else:
		return os.path.join(workflow.basedir, "scripts/standIN_files/ALT_missing.fasta")
		
rule indexFasta_asm2:
	input:
		assembly_asm2=asm2File,
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.fai")
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/indexASM.asm2.{asmID}.log")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['indexFasta_PRI']['threads']
	resources:
		mem_mb=resource['indexFasta_PRI']['mem_mb'],
		time=resource['indexFasta_PRI']['time']
	shell:
		"""
		(bwa-mem2 index {input.assembly_asm2}) &> {log}
		(samtools faidx {input.assembly_asm2}) &>> {log}
		"""

rule convertFastqTObam_R1_HiC_asm1:
	input:
		HiC_R1=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R1.fastq"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R1.bam"))
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['convertFastqTObam_R1_HiC']['threads']
	resources:
		mem_mb=resource['convertFastqTObam_R1_HiC']['mem_mb'],
		time=resource['convertFastqTObam_R1_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/fastq2bam.asm1.HiC.R1.{asmID}.log")
	shell:
		"""
		(bwa-mem2 mem -t {threads} -B8 {input.assembly} {input.HiC_R1} | samtools view -Sb - > {output})  &> {log}
		"""

rule convertFastqTObam_R1_HiC_asm2:
	input:
		HiC_R1=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R1.fastq"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R1.bam"))
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['convertFastqTObam_R1_HiC']['threads']
	resources:
		mem_mb=resource['convertFastqTObam_R1_HiC']['mem_mb'],
		time=resource['convertFastqTObam_R1_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/fastq2bam.asm2.HiC.R1.{asmID}.log")
	shell:
		"""
		(bwa-mem2 mem -t {threads} -B8 {input.assembly} {input.HiC_R1} | samtools view -Sb - > {output})  &> {log}
		"""

rule convertFastqTObam_R2_HiC_asm1:
	input:
		HiC_R2=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R2.fastq"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R2.bam"))
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['convertFastqTObam_R2_HiC']['threads']
	resources:
		mem_mb=resource['convertFastqTObam_R2_HiC']['mem_mb'],
		time=resource['convertFastqTObam_R2_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/fastq2bam.asm1.HiC.R2.{asmID}.log")
	shell:
		"""
		(bwa-mem2 mem -t {threads} -B8 {input.assembly} {input.HiC_R2} | samtools view -Sb - > {output})  &> {log}
		"""

rule convertFastqTObam_R2_HiC_asm2:
	input:
		HiC_R2=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.HiC.R2.fastq"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R2.bam"))
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['convertFastqTObam_R2_HiC']['threads']
	resources:
		mem_mb=resource['convertFastqTObam_R2_HiC']['mem_mb'],
		time=resource['convertFastqTObam_R2_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/fastq2bam.asm2.HiC.R2.{asmID}.log")
	shell:
		"""
		(bwa-mem2 mem -t {threads} -B8 {input.assembly} {input.HiC_R2} | samtools view -Sb - > {output})  &> {log}
		"""

rule filter5PrimeEnd_R1_HiC_asm1:
	input:
		HiC_R1_bam=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R1.bam"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R1.FILTERED.bam"))
	params:
		script=os.path.join(workflow.basedir, "scripts/process_HiC/filter_five_end.pl")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['filter5PrimeEnd_R1_HiC']['threads']
	resources:
		mem_mb=resource['filter5PrimeEnd_R1_HiC']['mem_mb'],
		time=resource['filter5PrimeEnd_R1_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/filtered.asm1.HiC.R1.{asmID}.log")
	shell:
		"""
		(samtools view -h {input.HiC_R1_bam}| perl {params.script} | samtools view -@{threads} -Sb - > {output}) &> {log}
		"""


rule filter5PrimeEnd_R1_HiC_asm2:
	input:
		HiC_R1_bam=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R1.bam"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R1.FILTERED.bam"))
	params:
		script=os.path.join(workflow.basedir, "scripts/process_HiC/filter_five_end.pl")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['filter5PrimeEnd_R1_HiC']['threads']
	resources:
		mem_mb=resource['filter5PrimeEnd_R1_HiC']['mem_mb'],
		time=resource['filter5PrimeEnd_R1_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/filtered.asm2.HiC.R1.{asmID}.log")
	shell:
		"""
		(samtools view -h {input.HiC_R1_bam}| perl {params.script} | samtools view -@{threads} -Sb - > {output}) &> {log}
		"""

rule filter5PrimeEnd_R2_HiC_asm1:
	input:
		HiC_R2_bam=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R2.bam"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R2.FILTERED.bam"))
	params:
		script=os.path.join(workflow.basedir, "scripts/process_HiC/filter_five_end.pl")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['filter5PrimeEnd_R2_HiC']['threads']
	resources:
		mem_mb=resource['filter5PrimeEnd_R2_HiC']['mem_mb'],
		time=resource['filter5PrimeEnd_R2_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/filtered.asm1.HiC.R1.{asmID}.log")
	shell:
		"""
		(samtools view -h {input.HiC_R2_bam}| perl {params.script} | samtools view -@{threads} -Sb - > {output}) &> {log}
		"""


rule filter5PrimeEnd_R2_HiC_asm2:
	input:
		HiC_R2_bam=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R2.bam"),
		assembly=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta"),
		indexes= [ os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.0123"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.amb"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.ann"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.bwt.2bit.64"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.pac"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta.fai") ]
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R2.FILTERED.bam"))
	params:
		script=os.path.join(workflow.basedir, "scripts/process_HiC/filter_five_end.pl")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['filter5PrimeEnd_R2_HiC']['threads']
	resources:
		mem_mb=resource['filter5PrimeEnd_R2_HiC']['mem_mb'],
		time=resource['filter5PrimeEnd_R2_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/filtered.asm2.HiC.R1.{asmID}.log")
	shell:
		"""
		(samtools view -h {input.HiC_R2_bam}| perl {params.script} | samtools view -@{threads} -Sb - > {output}) &> {log}
		"""


rule pairAndCombineFiltered_HiC_asm1:
	input:
		R1=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R1.FILTERED.bam"),
		R2=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.R2.FILTERED.bam")
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.COMBINED.FILTERED.bam"))
	params:
		script=os.path.join(workflow.basedir, "scripts/process_HiC/two_read_bam_combiner.pl")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['pairAndCombineFiltered_HiC']['threads']
	resources:
		mem_mb=resource['pairAndCombineFiltered_HiC']['mem_mb'],
		time=resource['pairAndCombineFiltered_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/combine.asm1.filtered.HiC.{asmID}.log")
	shell:
		"""
		(perl {params.script} {input.R1} {input.R2} | samtools view -@{threads} -Sb > {output}) &>{log}
		"""

rule pairAndCombineFiltered_HiC_asm2:
	input:
		R1=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R1.FILTERED.bam"),
		R2=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.R2.FILTERED.bam")
	output:
		temp(os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.COMBINED.FILTERED.bam"))
	params:
		script=os.path.join(workflow.basedir, "scripts/process_HiC/two_read_bam_combiner.pl")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['pairAndCombineFiltered_HiC']['threads']
	resources:
		mem_mb=resource['pairAndCombineFiltered_HiC']['mem_mb'],
		time=resource['pairAndCombineFiltered_HiC']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/combine.asm2.filtered.HiC.{asmID}.log")
	shell:
		"""
		(perl {params.script} {input.R1} {input.R2} | samtools view -@{threads} -Sb > {output}) &>{log}
		"""

rule pretextMap_asm1:
	input:
		HiC_alignment=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.COMBINED.FILTERED.bam")
	output:
		pretextFile=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.COMBINED.FILTERED.pretext")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['pretextMap']['threads']
	resources:
		mem_mb=resource['pretextMap']['mem_mb'],
		time=resource['pretextMap']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/PretextMap.{asmID}.asm1.log")
	shell:
		"""
		(samtools view -h {input.HiC_alignment} | PretextMap -o {output.pretextFile} --sortby length --mapq 10) &> {log}
		"""

rule pretextMap_asm2:
	input:
		HiC_alignment=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.COMBINED.FILTERED.bam")
	output:
		pretextFile=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.COMBINED.FILTERED.pretext")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['pretextMap']['threads']
	resources:
		mem_mb=resource['pretextMap']['mem_mb'],
		time=resource['pretextMap']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/PretextMap.{asmID}.asm2.log")
	shell:
		"""
		(samtools view -h {input.HiC_alignment} | PretextMap -o {output.pretextFile} --sortby length --mapq 10) &> {log}
		"""


rule pretextSnapshot_asm1:
	input:
		pretextFile=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.COMBINED.FILTERED.pretext")
	output:
		pretextSnapshotFULL=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.COMBINED.FILTERED_FullMap.png"),
	params:
		outDirectory=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['pretextSnapshot']['threads']
	resources:
		mem_mb=resource['pretextSnapshot']['mem_mb'],
		time=resource['pretextSnapshot']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/PretextSnapshot.{asmID}.asm1.log")
	shell:
		"""
		(PretextSnapshot -m {input.pretextFile} -o {params.outDirectory}) &> {log}
		"""


rule pretextSnapshot_asm2:
	input:
		pretextFile=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.COMBINED.FILTERED.pretext")
	output:
		pretextSnapshotFULL=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.COMBINED.FILTERED_FullMap.png"),
	params:
		outDirectory=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/")
	conda:
		os.path.join(workflow.basedir, "envs/HiC_CONTACT_MAPS.yaml")
	threads:
		resource['pretextSnapshot']['threads']
	resources:
		mem_mb=resource['pretextSnapshot']['mem_mb'],
		time=resource['pretextSnapshot']['time']
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/PretextSnapshot.{asmID}.asm2.log")
	shell:
		"""
		(PretextSnapshot -m {input.pretextFile} -o {params.outDirectory}) &> {log}
		"""

#######################################################################################################################################

def asm1_gzipped(wildcards):
	return yesGzip_asm1.loc[(wildcards.asmID), "PRI_asm"]


rule unzipFasta_asm1:
	input:
		assembly=asm1_gzipped,
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/pigzUnzip.asm1.{asmID}.log")
	conda:
		os.path.join(workflow.basedir, "envs/UNZIP_and_QC.yaml")
	threads:
		resource['unzipFasta_PRI']['threads']
	resources:
		mem_mb=resource['unzipFasta_PRI']['mem_mb'],
		time=resource['unzipFasta_PRI']['time']
	shell:
		"""
		pigz -p {threads} -c -d -k {input.assembly} > {output} 2> {log}
		"""

def asm1_unzipped(wildcards):
	return noGzip_asm1.loc[(wildcards.asmID), "PRI_asm"]

rule symlink_UnzippedFasta_asm1:
	input:
		assembly=asm1_unzipped,
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
	container:
		None
	shell:
		"""
		ln -fs {input} {output}
		"""

################

def asm2_gzipped(wildcards):
	return yesGzip_asm2.loc[(wildcards.asmID), "ALT_asm"]

rule unzipFasta_asm2:
	input:
		assembly=asm2_gzipped,
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta"),
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/pigzUnzip.asm2.{asmID}.log")
	conda:
		os.path.join(workflow.basedir, "envs/UNZIP_and_QC.yaml")
	threads:
		resource['unzipFasta_ALT']['threads']
	resources:
		mem_mb=resource['unzipFasta_ALT']['mem_mb'],
		time=resource['unzipFasta_ALT']['time']
	shell:
		"""
		pigz -p {threads} -c -d -k {input.assembly} > {output} 2> {log}
		"""


def asm2_unzipped(wildcards):
	return noGzip_asm2.loc[(wildcards.asmID), "ALT_asm"]

rule symlink_UnzippedFasta_asm2:
	input:
		assembly=asm2_unzipped,
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm2.fasta"),
	container:
		None
	shell:
		"""
		ln -s {input} {output}
		"""


####################



def merylDB(wildcards):
	return samples.loc[(wildcards.asmID), "merylDB"]

rule merqury:
	input:
		assembly1=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
		assembly2=asm2File,
		merylDB_provided=merylDB
	params:
		outFile= "{asmID}" + "_merqOutput",
		outPath= os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA"),
		symlink_merylDB=directory(os.path.join(config['Results'], "1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/merylDB_providedFor_{asmID}.meryl"))
	threads:
		resource['merqury']['threads']
	resources:
		mem_mb=resource['merqury']['mem_mb'],
		time=resource['merqury']['time']
	output:
		os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.qv"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.completeness.stats"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.st.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.fl.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.spectra-cn.st.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.spectra-cn.fl.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/merylDB_providedFor_{asmID}.hist")
	log:
		os.path.join(config['Results'],"1_evaluation/{asmID}/logs/merqury.{asmID}.log")
	priority:
		3
	conda:
		os.path.join(workflow.basedir, "envs/SMUDGEPLOT_MERQURY.yaml")
	shell:
		"""
		ln -fs {input.merylDB_provided} {params.symlink_merylDB}
		cd {params.outPath}
		export OMP_NUM_THREADS={threads}
		(merqury.sh {params.symlink_merylDB} {input.assembly1} {input.assembly2} {params.outFile}) &> {log}
		"""



#######################################################################################################################################

def busco_lineage_path(wildcards):
	lineage = samples.loc[(wildcards.asmID), "busco_lineage"]
	path = os.path.join(workflow.basedir, "buscoLineage/" + lineage + "_odb10")
	return path

def busco_output(wildcards):
	lineage = samples.loc[(wildcards.asmID), "busco_lineage"]
	path = os.path.join(config['Results'], "1_evaluation/" + wildcards.asmID + "/BUSCOs/" + wildcards.asmID + "/short_summary.specific." + lineage + "_odb10." + wildcards.asmID + ".txt")
	return path

def busco_output_asm(wildcards, asm): 
	lineage = samples.loc[(wildcards.asmID), "busco_lineage"]
	path = os.path.join(config['Results'], "1_evaluation/" + wildcards.asmID + "/BUSCOs/" + asm +  "/short_summary.specific." + lineage + "_odb10." + asm + ".txt")
	return path
	
def busco_lineage(wildcards):
	return samples.loc[(wildcards.asmID), "busco_lineage"]


rule busco5:
	input:
		assembly1=os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
		lineage_path = busco_lineage_path
	params:
		chngDir = os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs")
	threads:
		resource['busco5']['threads']
	resources:
		mem_mb=resource['busco5']['mem_mb'],
		time=resource['busco5']['time']
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs/asm1/busco.done")
	conda:
		os.path.join(workflow.basedir, "envs/BUSCO.yaml")
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/busco5.{asmID}_asm1.log")
	priority:
		20
	shell:
		"""
		cd {params.chngDir}
		(busco -m genome --offline --in {input.assembly1} -o asm1 -l {input.lineage_path} -c {threads} -f --limit 5) &> {log}
		
		touch {output}
		"""

rule busco5_asm2:
	input:
		assembly2 = asm2File,
		lineage_path = busco_lineage_path
	params:
		chngDir = os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs")
	threads:
		resource['busco5']['threads']
	resources:
		mem_mb=resource['busco5']['mem_mb'],
		time=resource['busco5']['time']
	output:
		os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs/asm2/busco.done")
	conda:
		os.path.join(workflow.basedir, "envs/BUSCO.yaml")
	log:
		os.path.join(config['Results'], "1_evaluation/{asmID}/logs/busco5.{asmID}_asm2.log")
	priority:
		20
	shell:
		"""
		cd {params.chngDir}
		(busco -m genome --offline --in {input.assembly2} -o asm2 -l {input.lineage_path} -c {threads} -f --limit 5) &> {log}
	
		touch {output}
		"""



rule make_kmer_histogram:
	output:
		hist=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/merylDB.hist")
	params:
		meryl_path=lambda wildcards: samples[samples['merylDB_basename'] == wildcards.name]['merylDB'].values[0]
	conda:
		os.path.join(workflow.basedir, "envs/SMUDGEPLOT_MERQURY.yaml")
	log:
		os.path.join(config['Results'],"1_evaluation/kmer_profiling/{name}/logs/make_kmer_histogram.log")
	threads:
		resource['merylHistogram']['threads']
	resources:
		mem_mb=resource['merylHistogram']['mem_mb'],
		time=resource['merylHistogram']['time']
	shell:
		"""
		(meryl histogram {params.meryl_path} > {output.hist}) &> {log}
		"""

rule genomescope2:
	input:
		hist=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/merylDB.hist")
	output:
		summary=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/results_summary.txt"),
		logPlot=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/results_log_plot.png"),
		linearPlot=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/results_linear_plot.png"),		
		estimatedSize=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/results_sizeEst.txt")
	params:
		outFolder=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/"),
		name_file="results",
		kmer=lambda wildcards: samples[samples['merylDB_basename'] == wildcards.name]['merylDB_kmer'].values[0],
		cpHist=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/merylDB_10000.hist")
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_R_SCRIPTS.yaml")
	log:
		os.path.join(config['Results'],"1_evaluation/kmer_profiling/{name}/logs/genomescope2.log")
	threads:
		resource['GenomeScope2Profiles']['threads']
	resources:
		mem_mb=resource['GenomeScope2Profiles']['mem_mb'],
		time=resource['GenomeScope2Profiles']['time']
	shell:
		"""
		head -n 10000 {input.hist} > {params.cpHist}
		(genomescope2 -i {params.cpHist} -o {params.outFolder} -k {params.kmer} -n {params.name_file}) &> {log}
		grep "Genome Haploid Length" {output.summary} | awk {{'print $6'}} | sed 's/,//g'> {output.estimatedSize}
		rm {params.cpHist}
		"""


rule smudgeplot:
	input:
		hist=os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/merylDB.hist")
	output:
		plot1=os.path.join(config['Results'],"1_evaluation/kmer_profiling/{name}/smudgeplot/results_smudgeplot.png"),
		plot2=os.path.join(config['Results'],"1_evaluation/kmer_profiling/{name}/smudgeplot/results_smudgeplot_log10.png"),
		summary_table=os.path.join(config['Results'],"1_evaluation/kmer_profiling/{name}/smudgeplot/results_summary_table.tsv")
	params:
		output_path=os.path.join(config['Results'],"1_evaluation/kmer_profiling/{name}/smudgeplot"),
		meryl_path=lambda wildcards: samples[samples['merylDB_basename'] == wildcards.name]['merylDB'].values[0],
		kmer=lambda wildcards: samples[samples['merylDB_basename'] == wildcards.name]['merylDB_kmer'].values[0]
	conda:
		os.path.join(workflow.basedir, "envs/SMUDGEPLOT_MERQURY.yaml")
	threads:
		resource['smudgeplot']['threads']
	resources:
		mem_mb=resource['smudgeplot']['mem_mb'],
		time=resource['smudgeplot']['time']
	log:
		os.path.join(config['Results'],"1_evaluation/kmer_profiling/{name}/logs/smudgeplot.log")
	shell:
		"""
		#meryl histogram {params.meryl_path} > {input.hist}
		
		L=$(smudgeplot.py cutoff {input.hist} L)
		U=$(smudgeplot.py cutoff {input.hist} U)

		(meryl print less-than ${{U}} greater-than ${{L}} threads={threads} memory={resources.mem_mb} {params.meryl_path} | sort > {params.output_path}/meryl_L${{L}}_U${{U}}.dump) &> {log}

		(smudgeplot.py hetkmers -o {params.output_path}/meryl_L${{L}}_U${{U}} --middle {params.output_path}/meryl_L${{L}}_U${{U}}.dump) &> {log}

		(smudgeplot.py plot -o {params.output_path}/results {params.output_path}/meryl_L${{L}}_U${{U}}_coverages.tsv -k {params.kmer} ) &> {log}
		"""


def merylDB_basename():
	unique_names = samples["merylDB_basename"].unique()
	return unique_names

rule symlink_genomescope:
	input:
		gscope=lambda wildcards: expand(os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/results_summary.txt"), name = merylDB_basename())
	output:
		os.path.join(config['Results'],"1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_summary.txt"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_log_plot.png"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_linear_plot.png"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_sizeEst.txt")
	params:
		merylDB_path=lambda wildcards: os.path.join(config['Results'], "1_evaluation/kmer_profiling", samples.loc[(wildcards.asmID), "merylDB_basename"]),
		output_path=lambda wildcards: os.path.join(config['Results'], "1_evaluation", wildcards.asmID)
	shell:
		"""
		# Create the target directories if they don't exist
		mkdir -p {params.output_path}/KMER_PROFILING/genomescope

		# Remove the symlinks if they already exist
		rm -rf {params.output_path}/KMER_PROFILING/genomescope/*

		# Symlink the directories
		ln -sf {params.merylDB_path}/genomescope/* {params.output_path}/KMER_PROFILING/genomescope/
		"""

rule symlink_smudgeplot:
	input:
		smudgeplot=expand(os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/smudgeplot/results_summary_table.tsv"), name = merylDB_basename())
	output:
		os.path.join(config['Results'],"1_evaluation/{asmID}/KMER_PROFILING/smudgeplot/results_summary_table.tsv"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/KMER_PROFILING/smudgeplot/results_smudgeplot.png")
	params:
		merylDB_path=lambda wildcards: os.path.join(config['Results'], "1_evaluation/kmer_profiling", samples.loc[(wildcards.asmID), "merylDB_basename"]),
		output_path=lambda wildcards: os.path.join(config['Results'], "1_evaluation", wildcards.asmID),
	shell:
		"""
		# Create the target directories if they don't exist
		mkdir -p {params.output_path}/KMER_PROFILING/smudgeplot

		# Remove the symlinks if they already exist
		rm -rf {params.output_path}/KMER_PROFILING/smudgeplot/*

		# Symlink the directories
		ln -sf {params.merylDB_path}/smudgeplot/* {params.output_path}/KMER_PROFILING/smudgeplot/
		"""


def readfile(file_path):
	with open(file_path) as f:
		ff=f.readlines()
	return ff[0][:-1]


rule gfastatsResults_asm1:
	input:
		assembly = os.path.join(config['Results'], "1_evaluation/{asmID}/ASSEMBLY_FASTAS/{asmID}.asm1.fasta"),
		estGenomeFile = os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_sizeEst.txt")
	output:
		gfaResults=os.path.join(config['Results'],"1_evaluation/{asmID}/ASSEMBLY_STATISTICS/asm1/{asmID}_gfastats.txt")
	params:
		estGenome = lambda wildcards: dictSamples[wildcards.asmID][6] if dictSamples[wildcards.asmID][6] != 0 else readfile(os.path.join(config['Results'], f"1_evaluation/{wildcards.asmID}/KMER_PROFILING/genomescope/results_sizeEst.txt"))
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_R_SCRIPTS.yaml")	
	log:
		os.path.join(config['Results'],"1_evaluation/{asmID}/logs/assemblyStats_gfastats_asm1.{asmID}.log")
	threads:
		resource['gfastatsResults']['threads']
	resources:
		mem_mb=resource['gfastatsResults']['mem_mb'],
		time=resource['gfastatsResults']['time']
	shell:
		"""
		(gfastats --nstar-report -j {threads} {input.assembly} {params.estGenome} > {output.gfaResults}) &> {log}
		"""

rule gfastatsResults_asm2:
	input:
		assembly = asm2File,
		estGenomeFile = os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_sizeEst.txt")
	output:
		gfaResults=os.path.join(config['Results'],"1_evaluation/{asmID}/ASSEMBLY_STATISTICS/asm2/{asmID}_gfastats.txt")
	params:
		estGenome = lambda wildcards: dictSamples[wildcards.asmID][6] if dictSamples[wildcards.asmID][6] != 0 else readfile(os.path.join(config['Results'], f"1_evaluation/{wildcards.asmID}/KMER_PROFILING/genomescope/results_sizeEst.txt"))
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_R_SCRIPTS.yaml")	
	log:
		os.path.join(config['Results'],"1_evaluation/{asmID}/logs/assemblyStats_gfastats_asm2.{asmID}.log")
	threads:
		resource['gfastatsResults']['threads']
	resources:
		mem_mb=resource['gfastatsResults']['mem_mb'],
		time=resource['gfastatsResults']['time']
	shell:
		"""
		(gfastats --nstar-report -j {threads} {input.assembly} {params.estGenome} > {output.gfaResults}) &> {log}
		"""


rule all_metrics_table_asm1:
	input:
		gfaStats1=os.path.join(config['Results'],"1_evaluation/{asmID}/ASSEMBLY_STATISTICS/asm1/{asmID}_gfastats.txt"),
		qv=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.qv"),
		busco_done = os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs/asm1/busco.done"),
		completeness=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.completeness.stats")
	output:
		all_metrics_table = os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_asm1.tsv")
	container:
		None
	params:
		busco_asm1=lambda wildcards: busco_output_asm(wildcards, "asm1"),
		script = os.path.join(workflow.basedir, "scripts/report/makeAllMetricsTable.sh")
	shell:
		"""
		{params.script} {wildcards.asmID} {input.gfaStats1} {input.qv} {params.busco_asm1} {input.completeness} 1 {output.all_metrics_table}
		"""	


rule all_metrics_table_asm2:
	input:
		gfaStats1=os.path.join(config['Results'],"1_evaluation/{asmID}/ASSEMBLY_STATISTICS/asm2/{asmID}_gfastats.txt"),
		qv=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.qv"), 
		busco_done = os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs/asm2/busco.done"),
		completeness=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.completeness.stats")
	output:
		all_metrics_table = os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_asm2.tsv")
	container:
		None
	params:
		busco_asm2=lambda wildcards: busco_output_asm(wildcards, "asm2"),
		script = os.path.join(workflow.basedir, "scripts/report/makeAllMetricsTable.sh")
	shell:
		"""
		{params.script} {wildcards.asmID} {input.gfaStats1} {input.qv} {params.busco_asm2} {input.completeness} 2 {output.all_metrics_table}
		"""	
	
rule KeyResults_GenomescopeProfiles:
	input:
		gscopeSum=os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_summary.txt"),
		gscopeLog=os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_log_plot.png"),
		gscopeLin=os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_linear_plot.png")
	output:
		gscopeSum=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}_genomescope_summary.txt"),
		gscopeLog=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}_genomescope_log_plot.png"),
		gscopeLin=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}_genomescope_linear_plot.png")
	container:
		None
	shell:
		"""
		cp {input.gscopeSum} {output.gscopeSum}
		cp {input.gscopeLog} {output.gscopeLog}
		cp {input.gscopeLin} {output.gscopeLin}
		"""

rule KeyResults_busco:
	input:
		busco_asm1 = os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs/asm1/busco.done"),
		busco_asm2 = lambda wildcards: os.path.join(config['Results'], "1_evaluation/{asmID}/BUSCOs/asm2/busco.done") if samples.loc[wildcards.asmID, "ALT_present"] else "scripts/standIN_files/ALT_missing.fasta",
	params:
		busco_asm1=lambda wildcards: busco_output_asm(wildcards, "asm1"),
		busco_asm2=lambda wildcards: busco_output_asm(wildcards, "asm2") if samples.loc[wildcards.asmID, "ALT_present"] else "scripts/standIN_files/ALT_missing.fasta",
		asm2provided=lambda wildcards: "true" if samples.loc[(wildcards.asmID), "ALT_present"] else "false",
		buscoScores_asm2=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/only_buscoScores_{asmID}_asm2.txt"),
		busco_asm2_output=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/short_summary.specific.{asmID}_asm2.txt")
	output:
		busco_asm1=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/short_summary.specific.{asmID}_asm1.txt"),
		buscoScores_asm1=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/only_buscoScores_{asmID}_asm1.txt")
	container:
		None
	shell:
		"""
		head -n 15 {params.busco_asm1} | tail -n 7 | sed "s/^['\t']*//" > {output.buscoScores_asm1}
		cp {params.busco_asm1} {output.busco_asm1}
		
		if {params.asm2provided}; then
			head -n 15 {params.busco_asm2} | tail -n 7 | sed "s/^['\t']*//" > {params.buscoScores_asm2}
			cp {params.busco_asm2} {params.busco_asm2_output}
		fi
		"""

rule KeyResults_merqury:
	input:
		qv=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.qv"),
		completeness=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.completeness.stats"),
		spectraStacked_asm1=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.st.png"),
		spectraFlat_asm1=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.fl.png"),
		spectraStacked_both=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.spectra-cn.st.png"),
		spectraFlat_both=os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.spectra-cn.fl.png")
	output:
		qv=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.qv"),
		completeness=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.completeness.stats"),
		spectraStacked_asm1=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.st.png"),
		spectraFlat_asm1=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.fl.png"),
		spectraStacked_both=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.spectra-cn.st.png"),
		spectraFlat_both=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.spectra-cn.fl.png")
	container:
		None
	shell:
		"""
		cp {input.completeness} {output.completeness}
		cp {input.qv} {output.qv}
		cp {input.spectraStacked_asm1} {output.spectraStacked_asm1}
		cp {input.spectraFlat_asm1} {output.spectraFlat_asm1}
		cp {input.spectraFlat_both} {output.spectraFlat_both}
		cp {input.spectraStacked_both} {output.spectraStacked_both}
		"""
rule KeyResults_smudgeplot:
	input: 
		smudgeplot=os.path.join(config['Results'],"1_evaluation/{asmID}/KMER_PROFILING/smudgeplot/results_summary_table.tsv"),
		smudgeplotPlot=os.path.join(config['Results'],"1_evaluation/{asmID}/KMER_PROFILING/smudgeplot/results_smudgeplot.png")
	output:
		smudgeplot=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/smudgeplot_summary_table.tsv"),
		smudgeplotPlot=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/results_smudgeplot.png")
	shell:
		"""
		cp {input.smudgeplot} {output.smudgeplot}
		cp {input.smudgeplotPlot} {output.smudgeplotPlot}
		"""
	
rule KeyResults_statistics:
	input:
		gscopeSum=os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_summary.txt"),
		all_metrics_table=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_asm1.tsv"),
		all_metrics_asm2 = lambda wildcards: os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_asm2.tsv") if samples.loc[wildcards.asmID, "ALT_present"] else "scripts/standIN_files/ALT_missing.fasta",
		gfastats_asm1=os.path.join(config['Results'],"1_evaluation/{asmID}/ASSEMBLY_STATISTICS/asm1/{asmID}_gfastats.txt"),
		gfastats_asm2=lambda wildcards: os.path.join(config['Results'],"1_evaluation/{asmID}/ASSEMBLY_STATISTICS/asm2/{asmID}_gfastats.txt") if samples.loc[wildcards.asmID, "ALT_present"] else "scripts/standIN_files/ALT_missing.fasta",
	params:
		asm2provided=lambda wildcards: "true" if samples.loc[(wildcards.asmID), "ALT_present"] else "false",
		run_smudgeplot= "true" if config['smudgeplot'] else "false",
		gfastats_asm2=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_gfastats_asm2.tsv"),
		busco=lambda wildcards: busco_output_asm(wildcards, "asm1"), 
		temp_all_metrics1=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_temp1.tsv"),
		temp_all_metrics2=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_temp2.tsv")
	output:
		gfastats_asm1=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_gfastats_asm1.tsv"),
		all_metrics_table = os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics.tsv"),
		all_metrics_table_values = os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_only_values.tsv")
	container:
		None
	shell:
		"""
		cp {input.gfastats_asm1} {output.gfastats_asm1}
		cp {input.all_metrics_table} {output.all_metrics_table}

		if {params.asm2provided}; then
			cp {input.gfastats_asm2} {params.gfastats_asm2}
			awk -F'\t' 'BEGIN {{OFS=FS}} NR == 1 {{$2 = $2 ".asm1"}} {{print}}' {input.all_metrics_table} > {params.temp_all_metrics1}
			awk -F'\t' 'BEGIN {{OFS=FS}} NR == 1 {{$2 = $2 ".asm2"}} {{print}}' {input.all_metrics_asm2} > {params.temp_all_metrics2}
			paste {params.temp_all_metrics1} <(cut -f 2- {params.temp_all_metrics2}) > {output.all_metrics_table}
			rm {params.temp_all_metrics1}
			rm {params.temp_all_metrics2}
		fi
		
		cut -f 2- {output.all_metrics_table} > {output.all_metrics_table_values}	
		"""

rule all_metrics_final_table:
	input:
		all_metrics_table=expand(os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_all_metrics_only_values.tsv"),  asmID=list(dictSamples.keys())),
		sampleSheet= config['samplesTSV'],
		config=os.path.join(workflow.basedir, "configuration/config.yaml")
	params:
		temp_header=os.path.join(config['Results'],"1_evaluation/finalResults/tables/temp.tsv")
	output:
		results=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_all_metrics.tsv"),
		newSampleSheet=os.path.join(config['Results'],"1_evaluation/finalResults/saved_configuration/savedSampleSheet.tsv"),
		newConfigFile=os.path.join(config['Results'],"1_evaluation/finalResults/saved_configuration/savedConfig.yaml")
	container:
		None
	shell:
		"""

		cp {input.sampleSheet} {output.newSampleSheet}
		cp {input.config} {output.newConfigFile}

		# Create a header for the concatenated file
		echo -e "ASM_ID\nTotal_bp\nGC_%\nScaf\nCont\nGaps\nGaps_bp\nGaps%\nGaps/Gbp\nLongest_scaf\nScaf_auN\nScaf_N50\nScaf_L50\nScaf_N90\nScaf_L90\nLongest_cont\nCont_auN\nCont_N50\nCont_L50\nCont_N90\nCont_L90\nqv\nKmer_Compl\nBUSCO_genes\nBUSCO_C\nBUSCO_S\nBUSCO_D\nBUSCO_F\nBUSCO_M" > {params.temp_header}
		paste {params.temp_header} {input.all_metrics_table} > {output.results}
		rm {params.temp_header}
		"""


rule IndividualResults_md:
	input:
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_gfastats_asm1.tsv"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}_genomescope_linear_plot.png"), 
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/results_smudgeplot.png") if config["smudgeplot"] else os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}_genomescope_log_plot.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.qv"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.completeness.stats"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/only_buscoScores_{asmID}_asm1.txt"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.spectra-cn.fl.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.spectra-cn.st.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.fl.png"),
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_merqOutput.{asmID}.asm1.spectra-cn.st.png"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KMER_PROFILING/genomescope/results_summary.txt")
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")
	params:
		"{asmID}",
		"{kmer}",
		busco_lineage,
		lambda wildcards: (samples.loc[(wildcards.asmID), "ALT_present"] == True),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}_gfastats_asm2.tsv"),
		os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/only_buscoScores_{asmID}_asm2.txt"),
		config['smudgeplot'],
		config['kmer_plot_flat']
	output:
		os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_k{kmer}_markdownForReport.md")
	script:
		os.path.join(workflow.basedir, "scripts/report/makePDF_indivMD.py")


rule PretextMaps_md:
	input:
		PretextMap_asm1=os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm1.HiC.COMBINED.FILTERED_FullMap.png"),
		PretextMap_asm2=lambda wildcards: os.path.join(config['Results'], "1_evaluation/{asmID}/HiC_MAPS/{asmID}.asm2.HiC.COMBINED.FILTERED_FullMap.png") if samples.loc[wildcards.asmID, "ALT_present"] else "scripts/standIN_files/ALT_missing.fasta"
	output:
		pretextCP2keyResults_asm1=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}.asm1.pretext_hiC_FullMap.png"),
		IndividualPretextMD=os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_PRETEXT.md")
	params:
		assemblyName='{asmID}',
		pretextCP2keyResults_asm2=os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/{asmID}.asm2.pretext_hiC_FullMap.png"),
		asm2provided=lambda wildcards: "true" if samples.loc[(wildcards.asmID), "ALT_present"] else False
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")
	script:
		os.path.join(workflow.basedir, "scripts/report/pretextMapsToMarkdown.py")


rule Table_md_all_metrics:
		input:
				results=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_all_metrics.tsv")
		output:
				os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_transpose.tsv")
		conda:
				os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")
		script:
				os.path.join(workflow.basedir, "scripts/report/addFullTableForReport_all_metrics.py")

rule ColouredTable_html:
	input:
		os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_transpose.tsv")
	params:
		os.path.join(workflow.basedir, "scripts/compare_results/colouredHeatmap_legend_metrics.tsv")
	output:
		os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_COLOURED.html"),
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_R_SCRIPTS.yaml")
	script:
		os.path.join(workflow.basedir, "scripts/compare_results/fullTable_heatmap_external_metrics.R")

rule HeatmapTable_html:
	input:
		os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_transpose.tsv")
	params:
		os.path.join(workflow.basedir, "scripts/compare_results/internalComparison_legend_metrics.tsv")
	output:
		os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_GRADIENT.html")
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_R_SCRIPTS.yaml")
	script:
		os.path.join(workflow.basedir, "scripts/compare_results/fullTable_heatmap_internalComparison_metrics.R")

rule BothTables_pdf:
	input:
		coloured=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_COLOURED.html"),
		gradient=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_GRADIENT.html")
	params:
		css=os.path.join(workflow.basedir, "scripts/report/tableOnSamePage.css")
	log:
		os.path.join(config['Results'], "1_evaluation/logs/ComparisonTables_createPDF.log")
	output:
		coloured=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_COLOURED.pdf"),
		gradient=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_GRADIENT.pdf")
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")
	shell:
		"""
		(pandoc -V papersize:a3 -V margin-top=1.5cm -V margin-left=1.5cm -V margin-right=0 -V margin-bottom=0 -c {params.css} -o {output.coloured} --pdf-engine=wkhtmltopdf {input.coloured}) &>> {log}
		(pandoc -V papersize:b3 -V margin-top=1.5cm -V margin-left=1.5cm -V margin-right=0 -V margin-bottom=0 -c {params.css} \
		-o {output.gradient} --pdf-engine=wkhtmltopdf --pdf-engine-opt="-O" --pdf-engine-opt="Landscape" {input.gradient}) &>> {log}
		"""

rule Reports_md:
	input:
		indivMD=[expand(os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_k{kmer}_markdownForReport.md"), asmID=key, kmer=value6) for key, [value1, value2, value3, value4, value5, value6, value7, value8, value9, value10, value11, value12, value13, value14, value15, value16, value17] in dictSamples.items()],
		landingPage=os.path.join(workflow.basedir, "scripts/report/reportLandingPage.md"),
		IndividualPretextMD=[expand(os.path.join(config['Results'],"1_evaluation/{asmID}/KEY_RESULTS/{asmID}_PRETEXT.md"), asmID=list(testDictPRETEXT.keys()))]
	output:
		FullMarkdown=os.path.join(config['Results'],"1_evaluation/finalResults/individual_reports/ALL_individual_REPORTS.md")
	container:
		None
	shell:
		"""
		cat {input.landingPage} {input.indivMD} {input.IndividualPretextMD} >> {output.FullMarkdown}
		"""

rule Reports_pdf:
	input:
		md_report=os.path.join(config['Results'],"1_evaluation/finalResults/individual_reports/ALL_individual_REPORTS.md")
	output:
		pdf_report=os.path.join(config['Results'],"1_evaluation/finalResults/individual_reports/ALL_individual_REPORTS.pdf")
	log:
		os.path.join(config['Results'], "1_evaluation/logs/ReportWithoutComparisonTables_createPDF.log")
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")
	shell:
		"""
		(pandoc -o {output.pdf_report} {input.md_report} --pdf-engine=tectonic) &>> {log}
		"""

rule ConcatAll_pdfs:
	input:
		pdf_report=os.path.join(config['Results'],"1_evaluation/finalResults/individual_reports/ALL_individual_REPORTS.pdf"),
		coloured=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_COLOURED.pdf"),
		gradient=os.path.join(config['Results'],"1_evaluation/finalResults/tables/TABLE_OF_RESULTS_GRADIENT.pdf")
	output:
		pdf_report=os.path.join(config['Results'],"1_evaluation/finalResults/GEP_FINAL_REPORT.pdf"),
	log:
		os.path.join(config['Results'], "1_evaluation/logs/ConcatAll_pdfs.log")
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")
	shell:
		"""
		(gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile={output.pdf_report} {input.pdf_report} {input.gradient} {input.coloured}) &>> {log}
		"""

rule compute_hic_coverage_ear:
	output:
		os.path.join(config['Results'], "1_evaluation/finalResults/hic_coverage_estimate.txt")
	params:
		asm2_provided = asm2_provided,
		asm1_path = last_asm1_path,
		asm2_path = last_asm2_path,
		hic1_path = hic1_path,
		hic2_path = hic2_path
	conda:	
		os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")	
	log:
		os.path.join(config['Results'],"1_evaluation/logs/hic_coverage_estimate_seqkit.log")
	threads:
		resource['compute_hic_coverage_ear']['threads']
	resources:
		mem_mb=resource['gfastatsResults']['mem_mb'],
		time=resource['gfastatsResults']['time']
	shell:
		"""
		# Get bases	
		HiC_BASES=$(seqkit stats --threads {threads} {params.hic1_path} {params.hic2_path} | awk 'NR > 1 {{gsub(",", "", $5); sum += $5}} END {{print sum}}')
		
		ASM_BASES=$(seqkit stats --threads {threads} {params.asm1_path} | awk 'NR > 1 {{gsub(",", "", $5); sum += $5}} END {{print sum}}')

		# Check if asm2 is provided
		if {params.asm2_provided}; then
			ASM2_BASES=$(seqkit stats --threads {threads} {params.asm2_path} | awk 'NR > 1 {{gsub(",", "", $5); sum += $5}} END {{print sum}}')
			# check for the largest asm
			if ((ASM_BASES < ASM2_BASES)); then
				ASM_BASES=$ASM2_BASES
			fi
		fi

		# Get coverage
		HIC_COV=$(awk "BEGIN {{printf \\"%.2f\\", $HiC_BASES / $ASM_BASES}}")
		# write a file with this value only
		echo $HIC_COV > {output}
		"""
		
rule compute_kmer_coverage_ear:
	input: 
		expand(os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/results_summary.txt"), name = merylDB_basename()) 
	output:
		os.path.join(config['Results'], "1_evaluation/finalResults/kmer_coverage_estimate.txt")
	params:
		path_model = os.path.join(config['Results'], f"1_evaluation/kmer_profiling/{samples['merylDB_basename'].unique()[0]}/genomescope/results_model.txt")
	shell:
		"""
		# Get kmer coverage
		VAR="$(grep -n "kmercov " {params.path_model} | cut -f1 -d:)"
		KCOV="$(printf "%.2f\\n" $(sed -n $VAR\p {params.path_model} | sed -e 's/ \\+/\\t/g' | cut -f2 | bc -l))"
		
		# write a file with this value only
		echo $KCOV > {output}
		"""


rule make_ear:
	input:
		expand(os.path.join(config['Results'], "1_evaluation/kmer_profiling/{name}/genomescope/results_summary.txt"), name = merylDB_basename()),
		[expand(os.path.join(config['Results'], "1_evaluation/{asmID}/KEY_RESULTS/short_summary.specific.{asmID}_asm1.txt"), asmID = key) for key, values in dictSamples.items()],
		[expand(os.path.join(config['Results'],"1_evaluation/{asmID}/QV.KMER-COMPLETENESS.CN-SPECTRA/{asmID}_merqOutput.qv"), asmID = key) for key, values in dictSamples.items()],
		[expand(os.path.join(config['Results'],"1_evaluation/{asmID}/ASSEMBLY_STATISTICS/asm1/{asmID}_gfastats.txt"), asmID = key) for key, values in dictSamples.items()],
		kmer_coverage_file = os.path.join(config['Results'], "1_evaluation/finalResults/kmer_coverage_estimate.txt"),
		hic_coverage_file = os.path.join(config['Results'], "1_evaluation/finalResults/hic_coverage_estimate.txt") if hic_data else "scripts/standIN_files/ALT_missing.fasta"
	output:
		os.path.join(config['Results'],"1_evaluation/finalResults/EAR_report.yaml")
	conda:
		os.path.join(workflow.basedir, "envs/AUXILIARY_PYTHON_SCRIPTS.yaml")
	params:
		samples_tsv_path = config['samplesTSV'],
		merylDB = samples["merylDB_basename"].unique()[0],
		busco = samples["busco_lineage"].unique()[0],
		results_folder = config['Results'],
		script = os.path.join(workflow.basedir, "scripts/report/make_erga_assembly_report.py"),
		smudgeplot =  config['smudgeplot'],
		hic_data = hic_data,
		hic_coverage_file = os.path.join(config['Results'], "1_evaluation/finalResults/hic_coverage_estimate.txt"),
	log:
		os.path.join(config['Results'], "1_evaluation/logs/ear_report.log")
	shell:
		"""
		if {params.hic_data}; then
			hic_coverage=$(cat {params.hic_coverage_file})
		else
			hic_coverage=0
		fi
		kmer_coverage=$(cat {input.kmer_coverage_file})
		python {params.script} {output} {params.samples_tsv_path} {params.merylDB} {params.busco} {params.results_folder} {params.smudgeplot} $hic_coverage $kmer_coverage
		"""
