#!/usr/bin/env python3

import shutil

with open(snakemake.output.IndividualPretextMD, "w") as out:
	shutil.copyfile(snakemake.input.PretextMap_asm1, snakemake.output.pretextCP2keyResults_asm1)
	print("", file=out)
	print("###",snakemake.params.assemblyName," HiC Heatmap (assembly 1)", file=out)
	print("![](", snakemake.input.PretextMap_asm1, "){ height=30% }", file=out)
	if snakemake.params.asm2provided:
		shutil.copyfile(snakemake.input.PretextMap_asm2, snakemake.params.pretextCP2keyResults_asm2)
		print("", file=out)
		print("###",snakemake.params.assemblyName," HiC Heatmap (assembly 2)", file=out)
		print("![](", snakemake.input.PretextMap_asm2, "){ height=30% }", file=out)

