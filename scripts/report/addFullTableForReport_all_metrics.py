#!/usr/bin/env python3

import pandas as pd
from tabulate import tabulate
samples=pd.read_csv(snakemake.input.results, dtype=str, index_col=0, delim_whitespace=True, skip_blank_lines=True)

sampleTransposed=samples.T

with open(snakemake.output[0], "w") as out_plain:
	print(tabulate(sampleTransposed.rename_axis('ASM_ID'), headers='keys',tablefmt="plain", showindex=True), file=out_plain)
