#!/usr/bin/env bash

#usage: ASM ID, gfastats, qv, busco, completeness, output

#asm_id="AssemblyX"
asm_id=$1
#gfastats="/scratch/galeov97/begendiv/bApuApu/results3/1_evaluation/AssemblyX.illu/ASSEMBLY_STATISTICS/AssemblyX.illu_gfastats.txt"
gfastats=$2
#qv="/scratch/galeov97/begendiv/bApuApu/results3/1_evaluation/AssemblyX.illu/QV.KMER-COMPLETENESS.CN-SPECTRA/AssemblyX.illu_merqOutput.qv"
qv=$3
#busco="/scratch/galeov97/begendiv/bApuApu/results3/1_evaluation/AssemblyX.illu/BUSCOs/AssemblyX.illu/short_summary.specific.aves_odb10.AssemblyX.illu.txt" 
busco=$4
#completeness="/scratch/galeov97/begendiv/bApuApu/results3/1_evaluation/AssemblyX.illu/QV.KMER-COMPLETENESS.CN-SPECTRA/AssemblyX.illu_merqOutput.completeness.stats"
completeness=$5
asm_number=$6
output=$7

echo -e "ASM_ID\t$asm_id" >> $output
total_bp="$(grep 'Total scaffold length:' "${gfastats}" | awk '{print $4}')"
echo -e "Total_bp\t$total_bp" >> $output
echo -e "GC_%\t$(grep 'GC content' "${gfastats}" | awk '{print $4}')" >> $output
echo -e "Scaf\t$(grep '# scaffolds' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Cont\t$(grep '# contigs' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Gaps\t$(grep '# gaps' "${gfastats}" | awk '{print $5}')" >> $output
gaps_bp="$(grep 'Total gap length' "${gfastats}" | awk '{print $6}')"
echo -e "Gaps_bp\t$gaps_bp" >> $output
echo -e "Gaps%\t$(awk "BEGIN { printf \"%.3f\", ($gaps_bp / $total_bp) * 100 }")" >> $output
echo -e "Gaps/Gbp\t$(awk "BEGIN { printf \"%.3f\", ($gaps_bp / $total_bp) *  1000000000 }")" >> $output
echo -e "Longest_scaf\t$(grep 'Largest scaffold' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Scaf_auN\t$(grep 'Scaffold auN:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Scaf_N50\t$(grep -m 1 'Scaffold N50:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Scaf_L50\t$(grep -m 1 'Scaffold L50:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Scaf_N90\t$(grep 'Scaffold N90:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Scaf_L90\t$(grep 'Scaffold L90:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Longest_cont\t$(grep 'Largest contig' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Cont_auN\t$(grep 'Contig auN:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Cont_N50\t$(grep -m 1 'Contig N50:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Cont_L50\t$(grep -m 1 'Contig L50:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Cont_N90\t$(grep 'Contig N90:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "Cont_L90\t$(grep 'Contig L90:' "${gfastats}" | awk '{print $3}')" >> $output
echo -e "qv\t$(head -n ${asm_number} ${qv} | tail -n 1 | awk '{print $4}')" >> $output
echo -e "Kmer_Compl\t$(awk {'print $5'} $completeness | head -n ${asm_number} | tail -n 1 )" >> $output
echo -e "BUSCO_genes\t$(grep 'Complete BUSCOs' ${busco} | awk '{print $1}')" >> $output
echo -e "BUSCO_C\t$(grep 'C:' ${busco} | awk -F'[:\\[,]' {'print $2'})" >> $output
echo -e "BUSCO_S\t$(grep 'S:' ${busco} | awk -F'[:\\[,]' {'print $4'})" >> $output
echo -e "BUSCO_D\t$(grep 'D:' ${busco} | awk -F'[:\\[,\\]]' {'print $6'})" >> $output
echo -e "BUSCO_F\t$(grep 'F:' ${busco} | awk -F'[:\\[,]' {'print $8'})" >> $output
echo -e "BUSCO_M\t$(grep 'M:' ${busco} | awk -F'[:\\[,]' {'print $10'})" >> $output

